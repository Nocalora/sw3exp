# README #



### What is this repository for? ###

This is the now official Code-Part of SW3.Expansion
This will include all current, and future code-updates.


### How do I get set up? ###

Get the Latest Code
Get Notepad++
Get some Time
...and you're good.

### THE TEAM ###

Nocalora: Creator of the Mod


```
#!lua

### CREDITS ###
~Devs
Elite Games Team (Star Wolves 3)
xBow Software (IMD Exporter for 3DS Max

~Modding Groups / Individual Users
StarRover Team / Valv_id / Michael_Moon (Retrospection, Orion and the Engine Update)
Goblin Wizard (Inspiration and his Motherships Mod)
Nanaki (Fleet Mod and additional help with scripting)
Trucidation (Missiles Mod)
Kotkatat/StarDoom (Testing, Suggestions, Ideas)
Neo Genesi (Testing, Suggestions, Ideas)
Lotrek (Played a huge part in Testing and helped me alot)
Zekito (Testing)
Crowfeathers (Testing)
Arclight (Testing)
Dark.Revenant (Sounds from the http://fractalsoftworks.com/forum/index.php?topic=8095.0 The Knights Templar Mod for StarSector)
Cycerin (Sounds from the http://fractalsoftworks.com/forum/index.php?topic=4018.0BlackRock Drive Yards Mod for StarSector)
Tyrotrela (Heavy testing)
ShadoWarrior (Testing)
MixXaz (Testing)
Blacky(Testing)
EmileKhadaji (Testing)
SARFAX (Testing)
Kolomans (Testing)
TooBig (Testing)
obimark (Testing)
AC37AC37 (Testing)

~Software
Autodesk (3DS MAX 2008)
Adobe (Photoshop)
```